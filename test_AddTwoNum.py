import unittest
import AddTwoNum

class TestCalc(unittest.TestCase):
    def test_add(self):
        self.assertEqual(AddTwoNum.add(10, 5), 15)
        self.assertEqual(AddTwoNum.add(-1, 1), 0)
        self.assertEqual(AddTwoNum.add(-1, -1), -2)
  
    
  
    
if __name__ == '__main__':
    unittest.main()
